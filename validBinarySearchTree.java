package com.codingInterviewPractice;

public class validBinarySearchTree {

    public static void main(String[] args) {
        Node node = new Node(5);
        node.left = new Node(4);
        node.right = new Node(7);
        System.out.println(new validBinarySearchTree().new Solution().isValidBST(node));
    }

    static class Node {
        int val;
        Node left;
        Node right;
        Node(int val) {
            this.val = val;
        }
    }

    class Solution {
        public boolean isValidBST(Node n) {
            return isValidBSTHelper(n, Integer.MIN_VALUE, Integer.MAX_VALUE);
        }

        public boolean isValidBSTHelper(Node n, int low, int high) {
            if (n == null) {
                return true;
            }
            int val = n.val;
            if ((val > low && val < high) &&
                    isValidBSTHelper(n.left, low, n.val) &&
                    isValidBSTHelper(n.right, n.val, high)) {
                return true;
            }
            return false;
        }
    }
}

package com.codingInterviewPractice;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class twoSum {
    public static class Solution {
        public int[] twoSum(int[] nums, int target) {
            for (int i1 = 0; i1 < nums.length; i1++) {
                for (int i2 = 0; i2 < nums.length; i2++) {
                    if (nums[i1] == nums[i2]) {
                        continue;
                    }
                    if (nums[i1] + nums[i2] == target) {
                        return new int[] {i1, i2};
                    }
                }
            }
            return null;
        }

        public int[] twoSumB(int[] nums, int target) {
            Map<Integer, Integer> values = new HashMap<>();
            for (int i = 0; i < nums.length; i++) {
                int diff = target - nums[i];
                if (values.containsKey(diff)) {
                    return new int[] {i, values.get(diff)};
                }
                values.put(nums[i], i);
            }
            return null;
        }
    }

    public static void main(String[] args) {
//        int[] nums = {2, 7, 11, 15};
//        int target = 18;
//        int[] result = new Solution().twoSumB(nums, target);
//        System.out.println(result[0] + ", " + result[1]);

        System.out.println(Arrays.toString(new Solution().twoSumB(new int[] {2, 7, 11, 15}, 18)));
    }
}

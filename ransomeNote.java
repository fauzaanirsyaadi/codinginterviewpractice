package com.codingInterviewPractice;

import java.util.HashMap;
import java.util.Map;

public class ransomeNote {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.canSpell("abcdef", "bed"));
        System.out.println(solution.canSpell("abcdef", "cat"));}

    public static class Solution {
        public boolean canSpell(String magazine, String note) {
            Map<Character, Integer> letters = new HashMap<>();
            for (char c : magazine.toCharArray()) {
                letters.put(c, letters.getOrDefault(c, 0) + 1);
            }

            for (char c : note.toCharArray()) {
                if (letters.getOrDefault(c, 0) == 0) {
                    return false;
                }
                letters.put(c, letters.get(c) - 1);
            }

            return true;
        }
    }

}

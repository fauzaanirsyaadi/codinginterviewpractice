package com.codingInterviewPractice;

import java.util.ArrayList;
import java.util.List;

public class Permutation {

    public List<List<Integer>> permute(int[] nums) {
        return permuteHelper(nums, 0);
    }

    public List<List<Integer>> permuteHelper(int[] nums, int start) {
        List<List<Integer>> result = new ArrayList<>();
        if (start == nums.length - 1) {
            List<Integer> list = new ArrayList<>();
            list.add(nums[start]);
            result.add(list);
            return result;
        }
        for (int i = start; i < nums.length; i++) {
            swap(nums, start, i);
            List<List<Integer>> temp = permuteHelper(nums, start + 1);
            for (List<Integer> list : temp) {
                list.add(nums[start]);
            }
            result.addAll(temp);
            swap(nums, start, i);
        }
        return result;
    }

    public void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    public static void main(String[] args) {
        Permutation permutation = new Permutation();
        System.out.println(permutation.permute(new int[] {1, 2, 3}));
    }
}

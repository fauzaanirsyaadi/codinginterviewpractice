package com.codingInterviewPractice;

public class addTwoNumbersAsALinkedList {

        public static void main(String[] args) {
            Node node1 = new Node(2);
            node1.next = new Node(4);
            node1.next.next = new Node(3);
            Node node2 = new Node(5);
            node2.next = new Node(6);
            node2.next.next = new Node(4);
            Node result = new addTwoNumbersAsALinkedList().new Solution().addTwoNumbers(node1, node2);
            while (result != null) {
                System.out.println(result.val);
                result = result.next;
            }
        }

        static class Node {
            int val;
            Node next;
            Node(int val) {
                this.val = val;
            }
        }

        class Solution {
            public Node addTwoNumbers(Node l1, Node l2) {
                Node result = new Node(0);
                Node current = result;
                int carry = 0;
                while (l1 != null || l2 != null) {
                    int x = (l1 != null) ? l1.val : 0;
                    int y = (l2 != null) ? l2.val : 0;
                    int sum = carry + x + y;
                    carry = sum / 10;
                    current.next = new Node(sum % 10);
                    current = current.next;
                    if (l1 != null) {
                        l1 = l1.next;
                    }
                    if (l2 != null) {
                        l2 = l2.next;
                    }
                }
                if (carry > 0) {
                    current.next = new Node(carry);
                }
                return result.next;
            }
        }
}
